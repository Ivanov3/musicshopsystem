// MusicShopSystem.cpp

#include <iostream>

// Parent classes
class Stock {
public:
    int stock;
    void restock() {}
    void updateStock() {}
    void salesReport() {}
};

class Item {
public:
    std::string title, author, genre, month;
    short year;
    void sell() {}
    void add() {}
    void loadFile() {}
    void saveFile() {}
};

// Child classes
class CD: public Item {
public:
};

class DVD: public Item {
public:
};

class Magazine: public Item {
public:
};

class Book: public Item {
public:
};

// Functions:
void displayMenu() {
    // Print Main Menu and prompt for selection
    std::cout << "------------------"
        << "\nMusic Shop System\n"
        << "------------------"
        << "\nMenu selection:\n"
        << "------------------\n"
        << "A. Sell items\n"
        << "B. Add items\n"
        << "C. Load file\n"
        << "D. Save file\n"
        << "E. Restock\n"
        << "F. Update Stock\n"
        << "G. Sales Report\n"
        << "H. Display Main Menu\n"
        << "X. Exit\n"
        << "------------------\n";
}

// Main
int main() {
    // Variables
    bool loopMenu = true;
    bool loopCheck;
    char selection, returnToMenu;

    // Objects
    CD folklore;
    folklore.title = "Folklore";
    folklore.author = "Taylor Swift";
    folklore.year = 2020;

    CD futureNostalgia;
    futureNostalgia.title = "Future Nostalgia";
    futureNostalgia.author = "Dua Lipa";
    futureNostalgia.year = 2020;

    CD psychodrama;
    psychodrama.title = "Psychodrama";
    psychodrama.author = "Dave";
    psychodrama.year = 2019;

    DVD tenet;
    tenet.title = "Tenet";
    tenet.genre = "Sci-fi/Drama";
    tenet.year = 2020;

    DVD theMule;
    theMule.title = "The Mule";
    theMule.genre = "Drama/Crime";
    theMule.year = 2018;

    Magazine vogueItalia;
    vogueItalia.title = "Vogue Italia";
    vogueItalia.month = "September";
    vogueItalia.year = 2020;

    Magazine vanityFair;
    vanityFair.title = "Vanity Fair";
    vanityFair.month = "January";
    vanityFair.year = 2021;

    Book mexicanGothic;
    mexicanGothic.title = "Mexican Gothic";
    mexicanGothic.author = "Silvia Moreno-Garcia";
    mexicanGothic.genre = "Horror Fiction";
    mexicanGothic.year = 2020;

    Book theSearcher;
    theSearcher.title = "The Searcher";
    mexicanGothic.author = "Tana French";
    theSearcher.genre = "Mystery";
    theSearcher.year = 2020;

    displayMenu();

    //std::cout << "Enter selection > ";
    //std::cin >> selection;

    while (loopMenu == true) {
        std::cout << "Enter selection > ";
        std::cin >> selection;
        if (selection == 'A' || selection == 'a') {
            // Sell Items
        }
        else if (selection == 'B' || selection == 'b') {
            // Add Items
        }
        else if (selection == 'C' || selection == 'c') {
            // Load file
        }
        else if (selection == 'D' || selection == 'd') {
            // Save file
        }
        else if (selection == 'E' || selection == 'e') {
            // Restock
        }
        else if (selection == 'F' || selection == 'f') {
            // Update Stock
        }
        else if (selection == 'G' || selection == 'g') {
            // Sales Report
        }
        else if (selection == 'H' || selection == 'h') {
            displayMenu(); // Display Main Menu
        }
        else if (selection == 'X' || selection == 'x') {
            return 0; // Exit
        }
        else {
            std::cout << "\nInvalid choice. Only letters from A to H accepted in main menu or X to quit.\n";
        }

        std::cout << "Would you like to return to main menu?";
        loopCheck = true;
        while (loopCheck == true) {
            std::cout << " [Y/N] > ";
            std::cin >> returnToMenu;
            if (returnToMenu == 'Y' || returnToMenu == 'y') {
                displayMenu();
                loopMenu = true;
                loopCheck = false;
            }
            else if (returnToMenu == 'N' || returnToMenu == 'n') {
                return 0; // Exit
            }
            else {
                std::cout << "Invalid choice. Enter Y to return or N to exit.";
                loopCheck = true;
            }
        }
    }
    return 0;
}
