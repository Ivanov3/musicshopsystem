#When MusicShopSystem.o changes, recompile it into the executable musicSystem
musicSystem: MusicShopSystem.o
	g++ MusicShopSystem.o -o musicSystem

#Create object file when MusicShopSystem.cpp changes
MusicShopSystem.o: MusicShopSystem.cpp
	g++ -c MusicShopSystem.cpp

#Remove other object files and MusicShopSystem
clean:
	rm *.o musicSystem